import java.util.Random;

public class App {

    public static void main(String[] args) {
        // Tạo một đối tượng của lớp Random
        Random randomDouble = new Random();

        // Tạo số ngẫu nhiên kiểu double từ 1-100
        double randomDoubleNumber = randomDouble.nextDouble() * 100 + 1;

        // In số ngẫu nhiên ra màn hình
        System.out.println("Số ngẫu nhiên kiểu double từ 1-100: " + randomDoubleNumber);

        Random randomInt = new Random();

        // Tạo số ngẫu nhiên kiểu int từ 1-10
        int randomIntNumber = randomInt.nextInt(10) + 1;

        // In số ngẫu nhiên ra màn hình
        System.out.println("Số ngẫu nhiên kiểu int từ 1-10: " + randomIntNumber);

    }
}
